import axios from 'axios';
import dompurify from 'dompurify';

function searchResultsHTML(people, searchValue) {
  let searchResult = people
    .filter(person => person.name.toLowerCase().includes(searchValue.toLowerCase()))

  if (searchResult.length > 0) {
    return searchResult.map(person => {
            return `<li>${person.name}</li>`
          })
          .join('');
  }
  return people.map(person => {
          return `<li>${person.name}</li>`
        })
        .join('');
}

function typeAhead(search) {
    const searchInput = search.querySelector('input[name="search"]');
    const searchResults = search.querySelector('.search__results');

    // show the search results
    axios
      .get(`/api/`)
      .then(res => {
        if (res.data.length) {
          console.log('we are fetching new data')
          searchInput.on('input', function() {
            searchResults.innerHTML = dompurify.sanitize(searchResultsHTML(res.data, this.value));
          });
        }
      })
      .catch(err => {
        console.error(err);
      });
}

export default typeAhead;
