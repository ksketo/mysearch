const express = require('express');
const router = express.Router();

const data = require('../exercise-data');

/* GET home page. */
router.get('/', function(req, res, next) {
  const names = data.map(person => person.name);
  res.render('index', {
    title: 'Search',
    names
  });
});

router.get('/api', function(req, res, next) {
  return res.json(data);
});

module.exports = router;
