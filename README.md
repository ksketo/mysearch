# MySearch

A real-time search tool that fetches names from a JSON file.

## How to run

```bash
# Install dependencies
npm install

# Start express server
npm start
```

Npm version used is 5.0.3 and Node version is 8.1.2.

Server by default runs on port 3000. To run on a different port just change the environmental variable PORT.

## Notes

- During development nodemon has been used in order to monitor any changes and restart the server automatically, without having to do the process manually.
- Webpack was used to create a bundle with all the code for the search functionality. The bundle is compressed.
- JSON data is fetched once on the server and once on the browser when the page is loaded. Once loaded all the filtering is done on the client. Alternatively every time a letter is typed, we could make an API request to the server and fetch only the filtered data, but this would result in increased latency due to client-server communication and also potential server overloading.
- The dompurify package was used to prevent XSS attacks through the search field.

## To-Do

- Implement a design for the UI
- Test front-end with Selenium (or add a front-end framework).
- Write tests for back-end in case additional functionality is implemented.
- Store json data in database and use it to fetch any queries - currently data is stored in RAM, which limits us to small JSON files.
- Fetch additional information from JSON, that is shown to the user when a name is clicked.
